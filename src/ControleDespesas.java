
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * @author Helielton
 */

/**
 * TEMA 3 - Sistema de controle de gastos pessoais: Um sistema que permita
estudantes da universidade controlarem seus gastos semanais. O sistema permite
conferência dos gastos realizados (que devem conter dia, local, valor e setor) por
listagem total (com os itens discriminados) e/ou valor total de gastos. O sistema
ainda oferece análise por setor de gasto (e.g. alimentação, transporte, vestuário,
supermercado, aluguel, telefonia, extra).
 */

public class ControleDespesas {

    public static void main(String[] arg) {
        Double alimentacao = 0.0, totalali = 0.0, transporte = 0.0, totaltrans = 0.0, vestuario=0.0, totalvest=0.0, mercado=0.0, totalmerc=0.0, aluguel=0.0, totalalug=0.0, telefone=0.0, totaltel=0.0, totaldesp=0.0;

        int opcao = 0, opcaodesp=0;
        
        while(opcao!= 4){
        System.out.println("__________________________________");
        System.out.println("Selecione uma das opcoes abaixo: ");
        System.out.println("1 - Lancar despesa");
        System.out.println("2 - Consultar despesa");
        System.out.println("3 - Consultar total despesa");
        System.out.println("4 - Sair");
        opcao = new Scanner(System.in).nextInt();
        
        
            switch(opcao) {
                case 1:
                    System.out.println("Informe a despesa que deseja lancar: 1. Alimentacao / 2.Transporte / 3.Vestuario / 4.Supermercado / 5.Aluguel / 6.Telefone / 7.Sair");
                    opcaodesp = new Scanner(System.in).nextInt();
                    
                    if (opcaodesp == 1) {
                        System.out.println("Informe o valor gasto com alimentacao: ");
                        alimentacao = new Scanner(System.in).nextDouble();
                        totalali = totalali + alimentacao;
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }else if (opcaodesp == 2) {
                        System.out.println("Informe o valor gasto com Transporte: ");
                        transporte = new Scanner(System.in).nextDouble();
                        totaltrans = totaltrans + transporte;
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }else if(opcaodesp == 3){
                        System.out.println("Informe o valor gasto com Vestuario: ");
                        vestuario = new Scanner(System.in).nextDouble();
                        totalvest = totalvest + vestuario;
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }else if(opcaodesp == 4){
                        System.out.println("Informe o valor gasto com Supermercado: ");
                        mercado = new Scanner(System.in).nextDouble();
                        totalmerc = totalmerc + mercado;
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }else if(opcaodesp == 5){
                        System.out.println("Informe o valor gasto com Aluguel: ");
                        aluguel = new Scanner(System.in).nextDouble();
                        totalalug = totalalug + aluguel;
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }else if(opcaodesp == 6){
                        System.out.println("Informe o valor gasto com Telefone: ");
                        telefone = new Scanner(System.in).nextDouble();
                        totaltel = totaltel + telefone;
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }
                    else 
                    break;
                case 2:
                    System.out.println("Informe a despesa que deseja consultar: 1. Alimentacao / 2.Transporte / 3.Vestuario / 4.Supermercado / 5.Aluguel / 6.Telefone / 7.Sair");
                    opcaodesp = new Scanner(System.in).nextInt();
                    
                    if (opcaodesp == 1) {
                        System.out.println("Valor gasto com alimentacao= "+totalali);
                        System.out.println("Pressione Enter para continuar");
                        try{System.in.read();}
                        catch(Exception e){}
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }else if (opcaodesp == 2) {
                        System.out.println("Valor gasto com transporte= "+totaltrans);
                        System.out.println("Pressione Enter para continuar");
                        try{System.in.read();}
                        catch(Exception e){}
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }else if(opcaodesp == 3){
                        System.out.println("Valor gasto com vestuario = "+totalvest);
                        System.out.println("Pressione Enter para continuar");
                        try{System.in.read();}
                        catch(Exception e){}
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }else if(opcaodesp == 4){
                        System.out.println("Valor gasto com supermercado = "+totalmerc);
                        System.out.println("Pressione Enter para continuar");
                        try{System.in.read();}
                        catch(Exception e){}
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }else if(opcaodesp == 5){
                        System.out.println("Valor gasto com aluguel = "+totalalug);
                        System.out.println("Pressione Enter para continuar");
                        try{System.in.read();}
                        catch(Exception e){}
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }else if(opcaodesp == 6){
                        System.out.println("Valor gasto com telefone = "+totaltel);
                        System.out.println("Pressione Enter para continuar");
                        try{System.in.read();}
                        catch(Exception e){}
                        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                        break;
                    }
                    else 
                    break;
                case 3:
                    totaldesp=totalali+totaltrans+totalvest+totalmerc+totalalug+totaltel;
                    System.out.println("Total gasto com alimentacao= "+totalali);
                    System.out.println("Total gasto com transporte= "+totaltrans);
                    System.out.println("Total gasto com vestuario= "+totalvest);
                    System.out.println("Total gasto com mercado= "+totalmerc);
                    System.out.println("Total gasto com aluguel= "+totalalug);
                    System.out.println("Total gasto com telefone= "+totaltel);
                    System.out.println("Total geral de despesas= "+totaldesp);
                    System.out.println("Pressione Enter para continuar");
                    try{System.in.read();}
                    catch(Exception e){}
                    System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                default:
                    break;
            }

        }
    }
}

